//
// Created by epw on 2021/10/14.
//

#include <iostream>
#include <locapic.h>
#include <config/version.h>

int main(int argc, char **argv) {
    std::cout << PROJECT_NAME << " " << PROJECT_VERSION << std::endl;

    const std::string url = argc > 1 ? argv[1] : "localhost:6547";
    std::cout << "server  " << url << std::endl;

    locapic::init_python_home("C:/src/locapi/locapic/release/vcpkg_installed/x64-windows/tools/python3/");
    locapic::append_sys_path("C:/src/locapi/locapip");
    locapic::init_logging("C:/src/locapi/client_test.txt");
    locapic::import_config(R"({"test": {}})");

    std::vector<std::string> requests;

    std::cout << "---------- Test unary_unary ----------" << std::endl;
    locapic::run_rpc(url, "test", "unary_unary",
                     {[](const std::string &) -> std::string {
                         std::string request = "{\n  \"text\": \"cpp_request\"\n}";
                         std::cout << "client send" << std::endl << request << std::endl;
                         return request;
                     }},
                     {[](const std::string &message) -> std::string {
                         std::cout << "client receive" << std::endl;
                         std::cout << message << std::endl;
                         return {};
                     }});

    std::cout << "---------- Test unary_stream ----------" << std::endl;
    locapic::run_rpc(url, "test", "unary_stream",
                     {[](const std::string &) -> std::string {
                         std::string request = "{\n  \"text\": \"cpp_request\"\n}";
                         std::cout << "client send" << std::endl << request << std::endl;
                         return request;
                     }},
                     {[](const std::string &message) -> std::string {
                         if (!message.empty()) {
                             std::cout << "client receive" << std::endl;
                             std::cout << message << std::endl;
                             return {};
                         } else {
                             std::cout << "client receive termination" << std::endl;
                             return {};
                         }
                     }});

    std::cout << "---------- Test stream_unary ----------" << std::endl;
    requests = {"{\n  \"text\": \"cpp_request stream 1\"\n}",
                "{\n  \"text\": \"cpp_request stream 2\"\n}",
                "{\n  \"text\": \"cpp_request stream 3\"\n}"};
    locapic::run_rpc(url, "test", "stream_unary",
                     {[&requests](const std::string &) -> std::string {
                         if (!requests.empty()) {
                             std::string request = requests.at(0);
                             requests.erase(requests.begin());
                             std::cout << "client send" << std::endl << request << std::endl;
                             return request;
                         } else {
                             std::cout << "client send termination" << std::endl;
                             return {};
                         }
                     }},
                     {[](const std::string &message) -> std::string {
                         std::cout << "client receive" << std::endl;
                         std::cout << message << std::endl;
                         return {};
                     }});

    std::cout << "---------- Test stream_stream ----------" << std::endl;
    requests = {"{\n  \"text\": \"cpp_request stream 1\"\n}",
                "{\n  \"text\": \"cpp_request stream 2\"\n}",
                "{\n  \"text\": \"cpp_request stream 3\"\n}"};
    locapic::run_rpc(url, "test", "stream_stream",
                     {[&requests](const std::string &) -> std::string {
                         if (!requests.empty()) {
                             std::string request = requests.at(0);
                             requests.erase(requests.begin());
                             std::cout << "client send" << std::endl << request << std::endl;
                             return request;
                         } else {
                             std::cout << "client send termination" << std::endl;
                             return {};
                         }
                     }},
                     {[](const std::string &message) -> std::string {
                         if (!message.empty()) {
                             std::cout << "client receive" << std::endl;
                             std::cout << message << std::endl;
                             return {};
                         } else {
                             std::cout << "client receive termination" << std::endl;
                             return {};
                         }
                     }});

    return 0;
}
