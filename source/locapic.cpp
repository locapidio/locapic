//
// Created by epw on 2021/10/14.
//

#include <cstdlib>
#include <iostream>
#include <pybind11/embed.h>
#include <locapic.h>

namespace py = pybind11;
using namespace py::literals;

std::string locapic::init_python_home(const std::string &python_home) {
    try {
        if (!python_home.empty()) {
            _putenv_s("PYTHONHOME", python_home.data());
        }
        static py::scoped_interpreter guard{};
    }
    catch (const std::exception &e) {
        std::cout << __FUNCTION__ << std::endl;
        std::cout << e.what() << std::endl;
        return e.what();
    }
    return {};
}

std::string locapic::init_logging(const std::string &path) {
    try {
        py::module_::import("locapip").attr("init_logging")(py::str(path));
    }
    catch (const std::exception &e) {
        std::cout << __FUNCTION__ << std::endl;
        std::cout << e.what() << std::endl;
        return e.what();
    }
    return {};
}

std::string locapic::log(const std::string &level, const std::string &text) {
    try {
        py::module_::import("logging").attr("level")(py::str(text));
    }
    catch (const std::exception &e) {
        std::cout << __FUNCTION__ << std::endl;
        std::cout << e.what() << std::endl;
        return e.what();
    }
    return {};
}

std::string locapic::append_sys_path(const std::string &path) {
    try {
        py::list sys_path = py::module_::import("sys").attr("path");
        sys_path.append(path);
    }
    catch (const std::exception &e) {
        std::cout << __FUNCTION__ << std::endl;
        std::cout << e.what() << std::endl;
        return e.what();
    }
    return {};
}

std::string locapic::import_config(const std::string &config_json) {
    try {
        py::dict config = py::module_::import("json").attr("loads")(py::str(config_json));
        py::module_::import("locapip").attr("config").attr("update")(config);
        py::module_::import("locapip").attr("import_config")();
    }
    catch (const std::exception &e) {
        std::cout << __FUNCTION__ << std::endl;
        std::cout << e.what() << std::endl;
        return e.what();
    }
    return {};
}

std::string locapic::run_rpc(const std::string &url, const std::string &proto, const std::string &rpc,
                             const std::vector<std::function<std::string(std::string)>> &request,
                             const std::vector<std::function<std::string(std::string)>> &response) {
    try {
        py::list py_request_argv;
        for (const std::function<std::string(std::string)> &f: request) {
            py_request_argv.append(py::cpp_function(f));
        }
        py::list py_response_argv;
        for (const std::function<std::string(std::string)> &f: response) {
            py_response_argv.append(py::cpp_function(f));
        }
        py::object run = py::module_::import("locapip").attr("run_rpc");
        run(url, proto, rpc, py_request_argv, py_response_argv);
    }
    catch (const std::exception &e) {
        std::cout << "url: " << url << std::endl;
        std::cout << "proto: " << proto << std::endl;
        std::cout << "rpc: " << rpc << std::endl;
        std::cout << e.what() << std::endl;
        return e.what();
    }
    return {};
}

std::string locapic::run_lpc(const std::string &module, const std::string &lpc,
                             const std::vector<std::function<std::string(std::string)>> &request,
                             std::string &response) {
    try {
        py::list py_request_argv;
        for (const std::function<std::string(std::string)> &f: request) {
            py_request_argv.append(py::cpp_function(f));
        }
        py::object run = py::module_::import("locapip").attr("run_lpc");
        response = run(module, lpc, py_request_argv).cast<std::string>();
    }
    catch (const std::exception &e) {
        std::cout << "module: " << module << std::endl;
        std::cout << "lpc: " << lpc << std::endl;
        std::cout << e.what() << std::endl;
        return e.what();
    }
    return {};
}
