//
// Created by epw on 2021/10/14.
//

#pragma once

#include <memory>
#include <string>
#include <vector>
#include <functional>

namespace locapic {
    /// start init_python_home interpreter
    /// \param python_home directory containing init_python_home executable
    /// \return exception text
    std::string init_python_home(const std::string &python_home = {});

    /// set logging file path
    /// \param path file path
    /// \return exception text
    std::string init_logging(const std::string &path);

    /// log something
    /// \param level FATAL/ERROR/WARNING/INFO/DEBUG
    /// \param text log content
    /// \return exception text
    std::string log(const std::string &level, const std::string &text);

    /// append system path
    /// \param path directory containing init_python_home package
    /// \return exception text
    std::string append_sys_path(const std::string &path);

    /// import_proto_builtin proto directory
    /// \param config_json directory containing (*.proto) protocol buffer files and (*.json) configuration files
    /// \return exception text
    std::string import_config(const std::string &config_json);

    /// async run_rpc rpc in package on server at url, see specification in each package.py
    /// \param url server address
    /// \param proto protocol buffer package
    /// \param rpc remote procedure call defined in package.proto
    /// \param request request functions passed into package.py
    /// \param response response functions passed into package.py
    /// \return exception text
    std::string run_rpc(const std::string &url, const std::string &proto, const std::string &rpc,
                        const std::vector<std::function<std::string(std::string)>> &request,
                        const std::vector<std::function<std::string(std::string)>> &response);

    /// sync run_lpc in module on local init_python_home environment
    /// \param module init_python_home module
    /// \param lpc local procedure call defined in module.py
    /// \param request request functions passed into lpc arguments
    /// \param response response message returned from lpc
    /// \return exception text
    std::string run_lpc(const std::string &module, const std::string &lpc,
                        const std::vector<std::function<std::string(std::string)>> &request,
                        std::string &response);
}
