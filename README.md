# C++嵌入式Python运行时

C++ embedded Python runtime

## 1 简介

locapic是一个内嵌Python解释器的C++库，封装且隔离了pybind11的一般接口，可以优雅地导入虚幻引擎项目。

需结合 [locapip](https://gitlab.com/6547/locapip) 部署。

## 2 编译

通过 [vcpkg](https://github.com/Microsoft/vcpkg) 编译依赖库

```shell
./vcpkg install pybind11 python3 --triplet x64-windows
```

下载源码仓库

```shell
cd YOUR_PATH
git clone https://gitlab.com/6547/locapic
```

用 [CLion](https://www.jetbrains.com/clion/) 打开```YOUR_PATH/locapic``` 目录，
参阅 [vcpkg-with-clion](https://github.com/Microsoft/vcpkg#vcpkg-with-clion) 设置```CMAKE_TOOLCHAIN_FILE```，
然后```Build``` ```Install```

## 3 导入UE项目

在```Build.cs```中添加以下定义，用你的实际路径替换```YOUR_PATH```

```csharp
if (Target.IsInPlatformGroup(UnrealPlatformGroup.Windows))
{
    PublicIncludePaths.Add("YOUR_PATH/locapic/install/Windows/include");
    
    PublicIncludePaths.Add("YOUR_PATH/locapic/install/Windows/include");
    
    foreach (string File in Directory.EnumerateFiles(
        "YOUR_PATH/locapic/install/Windows/lib", "*.lib", 
        SearchOption.TopDirectoryOnly))
    {
        PublicAdditionalLibraries.Add(File);
    }

    foreach (string File in Directory.EnumerateFiles(
        "YOUR_PATH/locapic/release/vcpkg_installed/x64-windows/lib", "*.lib", 
        SearchOption.TopDirectoryOnly))
    {
        PublicAdditionalLibraries.Add(File);
    }
}
```

## 4 示例